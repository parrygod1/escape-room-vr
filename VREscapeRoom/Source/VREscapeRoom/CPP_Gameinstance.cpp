// Fill out your copyright notice in the Description page of Project Settings.

#include "CPP_Gameinstance.h"
#include "GameFramework/InputSettings.h"

void UCPP_Gameinstance::changeMovementHandF(bool useLeftHand)
{
	UInputSettings* settings = UInputSettings::GetInputSettings();
	
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, this->useLeftHandSetting ? TEXT("True") : TEXT("False"));
	if(useLeftHand)
	{
		settings->RemoveAxisMapping(FInputAxisKeyMapping(TEXT("Move_Forward"), EKeys::OculusTouch_Right_Thumbstick_Y));
		settings->AddAxisMapping(FInputAxisKeyMapping(TEXT("Move_Forward"), EKeys::OculusTouch_Left_Thumbstick_Y));
		settings->RemoveAxisMapping(FInputAxisKeyMapping(TEXT("Move_Right"), EKeys::OculusTouch_Right_Thumbstick_X));
		settings->AddAxisMapping(FInputAxisKeyMapping(TEXT("Move_Right"), EKeys::OculusTouch_Left_Thumbstick_X));

		settings->RemoveAxisMapping(FInputAxisKeyMapping(TEXT("Rotate_Horizontal"), EKeys::OculusTouch_Left_Thumbstick_X));
		settings->AddAxisMapping(FInputAxisKeyMapping(TEXT("Rotate_Horizontal"), EKeys::OculusTouch_Right_Thumbstick_X));
		this->useLeftHandSetting = true;
		SaveConfig(CPF_Config, *GGameUserSettingsIni);
	}
	else
	{
		settings->RemoveAxisMapping(FInputAxisKeyMapping(TEXT("Move_Forward"), EKeys::OculusTouch_Left_Thumbstick_Y));
		settings->AddAxisMapping(FInputAxisKeyMapping(TEXT("Move_Forward"), EKeys::OculusTouch_Right_Thumbstick_Y));
		settings->RemoveAxisMapping(FInputAxisKeyMapping(TEXT("Move_Right"), EKeys::OculusTouch_Left_Thumbstick_X));
		settings->AddAxisMapping(FInputAxisKeyMapping(TEXT("Move_Right"), EKeys::OculusTouch_Right_Thumbstick_X));

		settings->RemoveAxisMapping(FInputAxisKeyMapping(TEXT("Rotate_Horizontal"), EKeys::OculusTouch_Right_Thumbstick_X));
		settings->AddAxisMapping(FInputAxisKeyMapping(TEXT("Rotate_Horizontal"), EKeys::OculusTouch_Left_Thumbstick_X));
		this->useLeftHandSetting = false;
		SaveConfig(CPF_Config, *GGameUserSettingsIni);
	}
}

bool UCPP_Gameinstance::loadHandMovementSetting()
{
	LoadConfig(GetClass(), *GGameUserSettingsIni);
	this->changeMovementHandF(this->useLeftHandSetting);
	return this->useLeftHandSetting;
}
