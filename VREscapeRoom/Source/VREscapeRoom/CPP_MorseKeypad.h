// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Engine/World.h"
#include "..\..\Plugins\VRExpansionPlugin\Source\VRExpansionPlugin\Public\Interactibles\VRButtonComponent.h"
#include "CPP_MorseKeypad.generated.h"

UCLASS()
class VRESCAPEROOM_API ACPP_MorseKeypad : public AActor
{
	GENERATED_BODY()
		
	float StartTime;
	float EndTime;

public:	
	// Sets default values for this actor's properties
	ACPP_MorseKeypad();
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
		UStaticMeshComponent* MorseMesh;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
		UVRButtonComponent* Button;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
		float DitEndTime = 0.3;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
		FString CorrectCode = TEXT(".....--.......-.-.");
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
		FString CurrentCode = TEXT("");
		

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	UFUNCTION(BlueprintCallable, Category = "CPP|Button")
		void InsertToCode(bool ButtonState);
	UFUNCTION(BlueprintImplementableEvent, Category = "CPP|Button")
		void OnDot();
	UFUNCTION(BlueprintImplementableEvent, Category = "CPP|Button")
		void OnDash();
	UFUNCTION(BlueprintImplementableEvent, Category = "CPP|Button")
		void OnCorrectCode();
	UFUNCTION(BlueprintImplementableEvent, Category = "CPP|Button")
		void OnEndCode();
};
