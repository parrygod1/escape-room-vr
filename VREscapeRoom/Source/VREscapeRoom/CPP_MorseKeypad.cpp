// Fill out your copyright notice in the Description page of Project Settings.


#include "CPP_MorseKeypad.h"

// Sets default values
ACPP_MorseKeypad::ACPP_MorseKeypad()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	MorseMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MorseStaticMesh"));
	Button = CreateDefaultSubobject<UVRButtonComponent>(TEXT("MorseButton"));
	Button->SetupAttachment(MorseMesh);
}

// Called when the game starts or when spawned
void ACPP_MorseKeypad::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACPP_MorseKeypad::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ACPP_MorseKeypad::InsertToCode(bool ButtonState)
{
	if (ButtonState == false)
	{
		EndTime = GetWorld()->GetTimeSeconds();
		float TimePressed = EndTime - StartTime;
		if (TimePressed >= 0.1 && TimePressed <= DitEndTime)
		{
			CurrentCode.Append(TEXT("."));
			OnDot();
		}
		else if(TimePressed > DitEndTime)
		{
			CurrentCode.Append(TEXT("-"));
			OnDash();
		}
	}
	else
	{
		StartTime = GetWorld()->GetTimeSeconds();
	}

	if (CurrentCode.Contains(TEXT(".-.-.")))
	{
		if (CurrentCode.Equals(CorrectCode))
		{
			//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Green, TEXT("WOW! You guessed the code!")); 
			OnCorrectCode();
		}
		else
		{
			//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, TEXT("WOW! You got the code totally wrong!"));
		}

		CurrentCode = TEXT("");
		OnEndCode();
	}

	if (CurrentCode.Len() > 18)
	{
		CurrentCode = TEXT("");
		OnEndCode();
	}

	//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, CurrentCode);
}