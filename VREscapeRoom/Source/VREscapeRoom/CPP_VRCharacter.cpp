// Fill out your copyright notice in the Description page of Project Settings.


#include "CPP_VRCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "HeadMountedDisplayFunctionLibrary.h"

TArray<FStaticMaterial> ACPP_VRCharacter::getMeshMaterials(UStaticMesh* referencedMesh)
{
	return referencedMesh->StaticMaterials;
}

void ACPP_VRCharacter::printSomeText(FString sText)
{
	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, sText);
}

void ACPP_VRCharacter::addForwardInput(float value)
{
	if (!this->bPCMode)
	{
		vForwardDirection = VRReplicatedCamera->GetForwardVector();
	}
	else
	{
		vForwardDirection = this->DebugCamera->GetForwardVector();
	}

	AddMovementInput(
		FVector(vForwardDirection.X, vForwardDirection.Y, 0),
		value
	);
}

void ACPP_VRCharacter::addRightInput(float value)
{
	if(UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
	{
		AddMovementInput(VRReplicatedCamera->GetRightVector(), value);
	}
	else
	{
		AddMovementInput(DebugCamera->GetRightVector(), value);
	}
}


void ACPP_VRCharacter::addHorizontalRotation(float value)
{
	const float rotationAngle = 45.0;
	const float delay = 0.2;
		
	if (FMath::Abs<float>(value) >= 0.8 && this->bCanRotate)
	{
		float rounded = FMath::RoundToFloat(value) * rotationAngle;
		VRMovementReference->PerformMoveAction_SnapTurn(rounded);
		this->bCanRotate = false;
		
		FTimerHandle UnusedHandle;
		GetWorldTimerManager().SetTimer(UnusedHandle, this, &ACPP_VRCharacter::onRotationDelay, delay, false);
	}
}

void ACPP_VRCharacter::onRotationDelay()
{	
	this->bCanRotate = true;
}

ACPP_VRCharacter::ACPP_VRCharacter()
{
	skmMesh = GetMesh();
	//bPCMode = UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled();
	DebugCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PCCamera"));
	DebugCamera->SetupAttachment(NetSmoother);

}

