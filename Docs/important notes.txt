1 unit in UE is = 1cm
so 0.70 units = 70cm
google real life dimensions for object scaling 

to skip plugin recompilation add bUsePrecompiled = true; in plugin's build.cs

if 3d widget buttons don't click/hover properly then go into player WidgetInteraction component
and change pointer index

C++ stuff:
- when declaring interface functions, arguments need to be referenced even if they are pointers
example:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category="CPP")
	void getHandPose(FTransform& relativeTransform);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category="CPP")
	void getGripObject(UObject*& object);

Those functions have only outputs in bp. If we want inputs too we need to add const before the arguments:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category="CPP")
	void getGripObject(const bool& isSlotGrip, UObject*& object); // here we have 1 input and 1 output